from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import landingpage
from .models import Status
from .forms import StatusForm
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


class LandingPageUnitTest(TestCase):
    def test_landingpage_url(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_landingpage_url_notexist(self):
        response = Client().get('')
        self.assertFalse(response.status_code==404)

    def test_landingpage_using_right_func(self):
        found = resolve('/')
        self.assertEqual(found.func, landingpage)

    def test_landingpage_using_right_html(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'landingpage.html')

    def test_landingpage_greeting_exist(self):
        request = HttpRequest()
        response = landingpage(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Halo, apa kabar?', html_response)

    def test_model_can_create_new_status(self):
        new_status = Status.objects.create(message= "istimewa!")
        counting_all_available_status = Status.objects.all().count()
        self.assertEqual(counting_all_available_status, 1)

    def test_form_validation_for_blank_items(self):
        form = StatusForm(data={'message': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['message'],
            ["This field is required."]
        )

    def test_lab5_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/', {'message': test})
        self.assertEqual(response_post.status_code, 302)

class LandingPageFunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(LandingPageFunctionalTest, self).setUp()


    def tearDown(self):
        self.browser.quit()
        super(LandingPageFunctionalTest, self).tearDown()

    def test_input_status(self):
        self.browser.get(self.live_server_url)
        new_message = self.browser.find_element_by_id('id_message')
        new_message.send_keys('Coba coba')
        time.sleep(4)
        submit = self.browser.find_element_by_id('submit')
        submit.send_keys(Keys.RETURN)
        time.sleep(4)
        self.browser.get(self.live_server_url)
        time.sleep(4)
        self.assertIn('Coba coba', self.browser.page_source)



        # Fill the form with data


        # submitting the form





# Create your tests here.
