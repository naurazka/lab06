from django.shortcuts import render,redirect
from .models import Status
from .forms import StatusForm


# Create your views here.

def landingpage(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')

    else:
        form = StatusForm()

    all_status = Status.objects.all().order_by('created_at').reverse()
    context = { 'form' :form,
                'all_status' : all_status}

    return render(request, 'landingpage.html', context)

