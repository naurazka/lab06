from django.db import models

class Status(models.Model):
	message       = models.CharField(max_length=300)
	created_at	  = models.DateTimeField(auto_now_add = True)
# Create your models here.
